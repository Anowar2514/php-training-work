<?php
include_once('./inheritance_class.php');
include_once('./inheritance_sub_class.php');

$obj01 = new inheritance_class('SONY');
$obj01->getMobileDetails();

$obj02 = new inheritance_class('iPhone');
$obj02->getMobileDetails();

$obj03 = new inheritance_sub_class('Symphony');
$obj03->getMobileDetails();
