@extends('admin.layouts.master')
@section('main_title','Users:')
@section('users','active')
@section('breadcrumbs','Users  /  Users List')
@section('content')
    <div class="content">
        <!-- HTML sourced data -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><b>User List</b></h5>
                <hr />
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <table class="table datatable-html">
                <thead>
        <tr>
            <th><b>Users Name</b></th>
            <th><b>Email Address</b></th>
            <th><b>Date & Time</b></th>
            <th class="text-center"><b>Actions</b></th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->created_at }}</td>
            <td class="text-center">
                <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{{ url('/admin/users/view') }}"><i class="icon-address-book"></i> View Profile</a></li>
                            <li><a href="#"><i class="icon-pen"></i> Edit Profile</a></li>
                            <li><a href="#"><i class="icon-alert"></i>Delete Profile</a></li>
                        </ul>
                    </li>
                </ul>
            </td>
        </tr>
            @endforeach
        </tbody>
    </table>
    {{ $users->links() }}
    <hr />
        </div>
    </div>
 @endsection
