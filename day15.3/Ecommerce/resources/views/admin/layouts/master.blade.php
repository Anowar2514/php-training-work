<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<title>@yield('main_title) | Dream Bird</title>--}}
    <title>@yield('main_title') Dream Bird</title>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/minified/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/minified/core.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/minified/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/minified/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/dashboard.js') }}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
    <!-- /theme JS files -->

</head>

<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png"  alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                <div class="dropdown-menu dropdown-content">
                    <div class="dropdown-content-heading">
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-sync"></i></a></li>
                        </ul>
                    </div>

                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>
            </li>
        </ul>

        <p class="navbar-text"><span class="label bg-success-400">Online</span></p>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/placeholder.jpg" alt="">
                    {{--<span>{{Auth::user()->name}}</span>--}}
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                    <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                                                                     onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="{{ url('/admin') }}" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                {{--<span class="media-heading text-semibold">{{Auth::user()->name}}</span>--}}
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp;Dhaka, BD
                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <li class="active"><a href="{{ url('/admin') }}"><i class="icon-home4"></i> <span>Home</span></a></li>
                            <li class="@yield('display_content')">
                                <a href="{{ url('dashboard') }}"><i class="icon-stack2"></i> <span>Display Section</span></a>
                                <ul>
                                    <li><a href="{{ url('admin/category/add') }}">Add Category</a></li>
                                    <li><a href="{{ url('admin/category/show') }}">Manage Category</a></li>
                                    <li class="navigation-divider"></li>
                                    <li><a href="{{ url('admin/product/add') }}" id="add">Add Products</a></li>
                                    <li><a href="../../layout_3/LTR/index.html" id="manage">Manage Products</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('/admin') }}"><i class="icon-copy"></i> <span>Advertisement</span></a>
                                <ul>
                                    <li><a href="../../layout_2/LTR/index.html" id="golden">Golden Category</a></li>
                                    <li><a href="../../layout_3/LTR/index.html" id="silver">Silver Category</a></li>
                                    <li><a href="../../layout_4/LTR/index.html" id="bronze">Bronze Category</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('/admin') }}"><i class="icon-basket"></i><span>Manufacturer Section</span></a>
                                <ul>
                                    <li><a href="boxed_default.html">Add Category</a></li>
                                    <li><a href="boxed_mini.html">Manage Category</a></li>
                                    <li class="navigation-divider"></li>
                                    <li><a href="boxed_default.html">Add Manufacturer</a></li>
                                    <li><a href="boxed_mini.html">Manage Manufacturer</a></li>
                                </ul>
                            </li>
                            <!-- Extensions -->
                            <li> <a href="{{ url('/admin') }}"><i class="icon-cash3"></i> <span>Invoice</span></a>
                                <ul>
                                    <li><a href="../../layout_2/LTR/index.html" id="order">Order List</a></li>
                                    <li><a href="../../layout_3/LTR/index.html" id="price">Price List</a></li>
                                    <li><a href="../../layout_4/LTR/index.html" id="income">Income</a></li>
                                </ul>
                            </li>
                            <li class="@yield('users')">
                                <a href="{{ url('/admin/dashboard') }}"><i class="icon-people"></i> <span>Users</span></a>
                                <ul>
                                    <li><a href="{{ url('/admin/users') }}" id="user_list">Users List</a></li>
                                    <li><a href="../../layout_4/LTR/index.html" id="user_query">Users Activity</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('/admin') }}"><i class="icon-people"></i> <span>Admins</span></a>
                                <ul>
                                    <li><a href="{{ url('/admin/admins') }}" id="user_list">Admins List</a></li>
                                    <li><a href="../../layout_4/LTR/index.html" id="user_query">Admins Activity</a></li>
                                </ul>
                            </li>
                            <li><a href="extension_blockui.html"><i class="icon-task"></i> <span>Inbox</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">@yield('main_title')</span>  Dream Bird</h4>
                    </div>

                    <div class="heading-elements">
                        <div class="heading-btn-group">
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                        </div>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                        <li class="active">@yield('breadcrumbs')</li>
                    </ul>

                    <ul class="breadcrumb-elements">
                        <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-gear position-left"></i>
                                Settings
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                                <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                                <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="content">
                  @yield('content')

                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2018. <a href="#">Dream Bird</a> by <a href="http://www.dream.bird.com" target="_blank">Md. Anowar Hossain</a>
                </div>
</div>
<!-- /page container -->

        </div>
    </div>
</div>
</body>
</html>
