@extends('admin.layouts.master')
@section('main_title','Add Category :')
@section('display_content','active')
@section('breadcrumbs','Display Section  /  Add Category')
@section('content')
    {!! Form::open(array('url' => 'admin/category/store'))!!}
    <hr />
    <h3 style="text-align: center"><b style="color:green">Category Submission</b></h3>
    <hr />
    <div class="form-group form-horizontal">
        <div class="form-group">
                {!! Form::label('title', 'Category Name :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('title', null, ['class' => 'form-control','placeholder'=>'Enter Category Title............']) !!}
            </div>
        </div>
        <div class="form-group">
                {!! Form::label('description', 'Description:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Textarea('description', null) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::submit('Add Category', ['class' =>  'btn btn-primary']) !!}
            </div>
        </div>
    </div>
    <hr /><hr />
    {!! Form::close() !!}
@endsection
