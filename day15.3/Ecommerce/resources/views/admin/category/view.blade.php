@extends('admin.layouts.master')
@section('main_title','Category View:')
@section('display_content','active')
@section('breadcrumbs','Display Section  /  Manage Category  /  View All Category Info')
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title" style="text-align: center"><b style="color: #1C005A">Category Record</b></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>
    <div class="content">
    <table class="table datatable-reorder">
        <thead>
        <tr>
            <th>Category Name</th>
            <th>Description</th>
            <th>Addition-Date & Time</th>
            <th>Action<th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $category->title }}</td>
            <td>{{ $category->description }}</td>
            <td>{{ $category->created_at }}</td>
            <td><a href="{{ url('/admin/category/show') }}" class="btn btn-primary">Back</a></td>
        </tr>
        </tbody>
    </table>
</div>
</div>
<!-- /basic column reorder -->
@endsection