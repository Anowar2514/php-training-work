@extends('admin.layouts.master')
@section('main_title','Category List :')
@section('display_content','active')
@section('breadcrumbs','Display Section  /  Manage Category')
@section('content')
    <div class="content">
        <!-- HTML sourced data -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><b>Category List</b></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <table class="table datatable-html">
                <thead>
                <tr>
                    <th><b>Category Name</b></th>
                    <th><b>Store Date-Time</b></th>
                    <th class="text-center"><b>Actions</b></th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                <tr>
                    <td>{{ $category->title }}</td>
                    <td>{{ $category->created_at }}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="active"><a href="{{ url('/admin/categories',$category->id) }}"><i class="icon-eye"></i> View</a></li>
                                    <li class=""><a href="{{ '/admin/categories/'.$category->id.'/edit'}}"><i class="icon-pencil"></i> Edit</a></li>
                                    {!! Form::open(array('url' => '/admin/category/delete/'.$category->id)) !!}
                                    {!! Form::submit('Delete',['class' => 'icon-trash']) !!}
                                    {!! Form::close() !!}
                                    {{--<li class="active"><a href="{{ '/admin/categories'.$category->id }}"><i class="icon-trash"></i>Delete</a></li>--}}
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $categories->links() }}
        </div>
    </div>
@endsection
