@extends('admin.layouts.master')
@section('main_title','Edit Category :')
@section('display_content','active')
@section('breadcrumbs','Display Section  /  Manage Category  /  Edit Category')
@section('content')
    {!! Form::Model($category, array('url' => 'admin/category/update/'.$category->id))!!}
    <hr />
    <h3 style="text-align: center"><b style="color:green">Edit Category</b></h3>
    <hr />
    <div class="form-group form-horizontal">
        <div class="form-group">
            {!! Form::label('title', 'Category Name :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('title', null, ['class' => 'form-control','placeholder'=>'Enter Category Title............']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Textarea('description', null) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::submit('Update Category', ['class' =>  'btn btn-success']) !!}
            </div>
        </div>
    </div>
    <hr /><hr />
    {!! Form::close() !!}
@endsection
