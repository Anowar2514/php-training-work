@extends('admin.layouts.master')
@section('main_title','Add Product :')
@section('content')
    {!! Form::open(array('url' => 'admin/category/store'))!!}
    <hr />
    <h3 style="text-align: center"><b style="color:#00b3ee">Add Product</b></h3>
    <hr />
    <div class="form-group form-horizontal">
        <div class="form-group">
            {!! Form::label('title', 'Product Name :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('title', null, ['class' => 'form-control','placeholder'=>'Enter Product Title............']) !!}
            </div>
        </div>
        <div class="form-group">

            {!! Form::label('category', 'Category Name :', ['class' => 'col-md-4 control-label']) !!}

            <div class="col-md-6">
                <select class="form-group">
                    <option value="">------------Select Category-----------------</option>
                    @foreach($categories as $category)
                    <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <div class="form-group">
            {!! Form::label('brand', 'Brand Name :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('brand', ['select' => '-------------- Select Brand --------------','Male' => 'Male', 'Female' => 'Female','Electronics' => 'Electronics', 'Dress' => 'Dress'], null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('product_price', 'Product price :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Number('product_price', null, ['class' => 'form-control','placeholder'=>'Enter Product Price............']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('product_quantity', 'Product Quantity :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Number('product_quantity', null, ['class' => 'form-control','placeholder'=>'Enter Product Quantity............']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('product_code', 'Product Code :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('product_code', null, ['class' => 'form-control','placeholder'=>'Enter Product Code............']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('product_short_description', 'Product Short Description:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Textarea('product_short_description', null) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('product_long_description', 'Product Long Description:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Textarea('product_long_description', null) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('product_image', 'Product Image :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('product_image') !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('publication_status', 'Publication Status :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('publication_status', ['select' => '-------------- Select Status --------------','0' => 'Published', '1' => 'Unpublished'], null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::submit('Add Product', ['class' =>  'btn btn-primary']) !!}
            </div>
        </div>
    </div>
    <hr /><hr />
    {!! Form::close() !!}
@endsection
