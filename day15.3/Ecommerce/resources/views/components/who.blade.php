@if(Auth::guard('web')->check())
    <p class="text-success">
        <strong>You are logged in as a <b style="color: darkgreen">User</b></strong>.
    </p>
    @else
    <p class="text-danger">
        <strong>You are logged out as a <b style="color: darkred">User</b></strong>.
    </p>
    @endif

@if(Auth::guard('admin')->check())
    <p class="text-success">
        <strong>You are logged in as a <b style="color: darkgreen">Admin</b></strong>.
    </p>
@else
    <p class="text-danger">
        <strong>You are logged out as a <b style="color: darkred">Admin</b></strong>.
    </p>
@endif