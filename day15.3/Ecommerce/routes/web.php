<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/index', function () {
    return view('index');
});
Route::get('/admin/users','UserController@index');
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/users/logout','Auth\LoginController@userLogout')->name('user.logout');

Route::prefix('admin')->group( function(){
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');

    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/category/add','Admin\CategoryController@create');
    Route::post('/category/store','Admin\CategoryController@store');
    Route::get('/category/show','Admin\CategoryController@index');

    Route::get('product/add','Admin\ProductController@create');
    Route::get('/users/view','UserController@create');
    Route::get('/categories/{id}','Admin\CategoryController@show');
    Route::get('/categories/{id}/edit','Admin\CategoryController@edit');
    Route::post('/category/update/{id}','Admin\CategoryController@update');
    Route::post('/category/delete/{id}','Admin\CategoryController@destroy');
});

Route::get('/dashboard','Admin\AdminController@getDashboard');


