<?php
/* Split an array into chunks of two:  array_chunk(array,size);*/
$cars = array('X-Corolla','Pazaro','BMW','Ferrari');
echo '<pre>';
print_r(array_chunk($cars,2));
echo '<br>';

/* Split an array into chunks of two and preserve the original keys: array_chunk(array,size,preserve_key); */

$car = array('X-Corolla'=>'White','Pazaro'=>'Green','BMW'=>'Blue','Ferrari'=>'Black');
echo '<pre>';
print_r(array_chunk($car,2,true));