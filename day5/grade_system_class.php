<?php

class grade_system_class
{
    public function studentGrade($bangla, $english, $math, $ict, $bangladesh)
    {

        $subjects = array($bangla, $english, $math, $ict, $bangladesh);
        foreach ($subjects as $name => $subject) {
            switch ($subject) {
                case ($subject >= 80 && $subject <= 100):
                    // $grade = 'A+';
                    $point = 5;
                    break;
                case ($subject >= 70 && $subject <= 79):
                    //$grade = 'A';
                    $point = 4;
                    break;
                case ($subject >= 60 && $subject <= 69):
                    // $grade = 'A-';
                    $point = 3.50;
                    break;
                case ($subject >= 50 && $subject <= 59):
                    // $grade = 'B';
                    $point = 3;
                    break;
                case ($subject >= 40 && $subject <= 49):
                    //$grade = 'C';
                    $point = 2;
                    break;
                case ($subject >= 33 && $subject <= 39):
                    //$grade = 'D';
                    $point = 1.50;
                    break;
                default:
                    //$grade = 'F';
                    $point = 0.00;
            };

            $grading = (($subjects[0] = $point) + ($subjects[1] = $point) + ($subjects[2] = $point) + ($subjects[3] = $point) + ($subjects[4] = $point)) / 5;
            return $grading;

        }
    }

    public function grade($bangla,$english,$math,$ict,$bangladesh)
    {
        $subjects = array($bangla,$english,$math,$ict,$bangladesh);
        foreach ($subjects as $name => $subject) {
            switch ($subject) {
                case ($subject >= 80 && $subject <= 100):
                    return $grade = 'A+';
                    // $point = 5;
                    break;
                case ($subject >= 70 && $subject <= 79):
                    return $grade = 'A';
                    //$point = 4;
                    break;
                case ($subject >= 60 && $subject <= 69):
                    return $grade = 'A-';
                    //$point = 3.50;
                    break;
                case ($subject >= 50 && $subject <= 59):
                    return $grade = 'B';
                    //$point = 3;
                    break;
                case ($subject >= 40 && $subject <= 49):
                   return $grade = 'C';
                    //$point = 2;
                    break;
                case ($subject >= 33 && $subject <= 39):
                    return $grade = 'D';
                    //$point = 1.50;
                    break;
                default:
                   return $grade = 'F';
                //$point = 0.00;
            };


        }

    }
}