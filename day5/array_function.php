<?php
// array for index value.
$car = array("Volvo","BMW","Tyota");
echo "I like ".$car[0].", ".$car[1].", ".$car[2].".<br>";

// array for associative value.
$age=array("Anowar"=>"24","Monia"=>"24","Ayan"=>"2");
echo "Our family age: Anowar age is =  ".$age['Anowar'].", Monia age is = ".$age['Monia'].", Ayan age is = ".$age['Ayan'].".";
echo '<br>';

//Loop through and print all the values of an indexed array
$cars = array('BMW','X-Corolla','Ferrari');
$arrlength = count($cars);

for($x=0; $x<$arrlength; $x++){
echo $cars[$x];
echo '<br />';
}

/* Loop through and print all the values of an associative array */

$birds = array("Parrot"=>"Green","Crow"=>"Black","Owl"=>"White");

foreach ( $birds as $bird => $color){
    echo 'Birds Name '.$bird. ' & Color is '.$color.'<br>';
}

/* Multidimensional array */
// A two dimensional array example
$bulb = array(
    array('White',100,50),
    array('Blue',300,100),
    array('Green',50,35)
);
echo 'Bulb color is '.$bulb[0][0].' Quantity is '.$bulb[0][1].' & Price is '.$bulb[0][2].'<br>';
echo 'Bulb color is '.$bulb[1][0].' Quantity is '.$bulb[1][1].' & Price is '.$bulb[1][2].'<br>';
echo 'Bulb color is '.$bulb[2][0].' Quantity is '.$bulb[2][1].' & Price is '.$bulb[2][2].'<br>';