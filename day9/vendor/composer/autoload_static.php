<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8e02714e4d87f450cb9f4d3a01f0101e
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8e02714e4d87f450cb9f4d3a01f0101e::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8e02714e4d87f450cb9f4d3a01f0101e::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
