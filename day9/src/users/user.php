<?php

namespace App\users;
use PDO;
class user
{
public $username ="";
public $password ="";
public $datetime ="";
public function setData($data){
    date_default_timezone_set('Asia/Dhaka');
    $this->datetime = date("y/m/d h:i:sa");
    $this->username = $data['username'];
    $this->password = $data['password'];
}

public function store(){
     try{
         $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
         $query = "INSERT INTO users(username,password,created_at) VALUES(:anyuser,:anypass,:created_at)";
         $stmt = $pdo->prepare($query);
         $stmt->execute(array(
             ':anyuser'=>$this->username,
             ':anypass'=>$this->password,
             ':created_at'=>$this->datetime
         ));
     }catch (PDOException $e){
         echo 'Error:'.$e->getMessage();
     }
}
public function get(){

}
}