<?php

namespace App\users;
use PDO;
class user
{
   public $fullname ="";
   public $username ="";
   public $email_address="";
   public $password ="";
   public $datetime ="";
   public $id ="";
   public function __construct()
   {
       session_start();
   }

    public function setData($data){

    date_default_timezone_set('Asia/Dhaka');
    $this->datetime = date("y/m/d h:i:sa");
    if(!empty($data['fullname'])) {
        $this->fullname = $data['fullname'];
    }
    if(!empty($data['username'])) {
        $this->username = $data['username'];
    }
    if(!empty($data['email_address'])) {
        $this->email_address = $data['email_address'];
    }
    if(!empty($data['password'])) {
        $this->password = $data['password'];
    }
    if(!empty($data['id'])) {
        $this->id = $data['id'];
    }
}

public function store(){
    try{
         $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
         $query = "INSERT INTO users(fullname,username,email_address,password,created_at) VALUES(:fullname,:anyuser,:email_address,:password,:created_at)";
         $stmt = $pdo->prepare($query);
         $stmt->execute(array(
             ':fullname'=>$this->fullname,
             ':anyuser'=>$this->username,
             ':email_address'=>$this->email_address,
             ':password'=>$this->password,
             ':created_at'=>$this->datetime
         ));
         if($stmt){
             $_SESSION['userSuccess']='User Successfully Added !!!!';
             header('Location:index.php');
         }
         else{
             echo 'Error in the database !!!';
         }
     }catch (PDOException $e){
         echo 'Error:'.$e->getMessage();
     }
}
public function getAllUsers(){
       try{
        $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
        $query = "SELECT * FROM users WHERE status = '1'";
        $stmt = $conn->query($query);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }catch (PDOException $e){
        echo 'Error:'.$e->getMessage();
    }
}
public function view($id=''){
    try{
        $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
        $query = "SELECT * FROM users WHERE id = '$id'";
        $stmt = $conn->query($query);
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        //$this->dd($data);
        return $data;
    }catch (PDOException $e){
        echo 'Error:'.$e->getMessage();
    }
}
public function update(){
    $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
    $query = "UPDATE users SET fullname = :fullname,username = :username,email_address = :email_address WHERE id=$this->id";
    $stmt = $pdo->prepare($query);
    $stmt->execute(array(
        ':fullname'=>$this->fullname,
        ':username'=>$this->username,
        ':email_address'=>$this->email_address
    ));
    if($stmt){
        $_SESSION['userSuccess']='User Successfully Updated !!!!';
        header('Location:index.php');
    }
    else{
        $_SESSION['userSuccess'] = 'Update is not completed !!!';
        header('Location:index.php');
    }
}
public function delete($id=''){
    $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
    $query = "DELETE * FROM users WHERE id=$id";
    $stmt = $pdo->query($query);
    if($stmt){
        $_SESSION['userSuccess'] ='User Successfully Deleted !!!!';
        header('Location:index.php');
    }
    else{
        $_SESSION['userSuccess'] = 'Delete is not completed !!!';
        header('Location:index.php');
    }
}
public function dd($data=''){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die();
}
}