<?php
include_once ("../../vendor/autoload.php");
use App\users\user;
$obj = new user();
$data = $obj->getAllUsers();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>List of users</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <hr/>
    <h2 style="text-align: center"><b>List of users</b></h2>
    <h3 style="color: darkblue">
        <?php
       if(isset($_SESSION['userSuccess'])){
           echo $_SESSION['userSuccess'];
           unset($_SESSION['userSuccess']);
       }
        ?>
    </h3>
    <hr/>
    <a href="create.php" class="btn btn-success">Add New User</a>
    <br><br>
    <hr/>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Serial No</th>
            <th>Full Name</th>
            <th>Email Address</th>
            <th></th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 1;
        foreach ($data as $item){
        ?>
        <tr>
            <td><?php echo $sl++;?></td>
            <td><?php echo $item['fullname'] ;?></td>
            <td><?php echo $item['email_address'] ;?></td>
            <td><a href="view.php?id=<?php echo $item['id'];?>" class="btn btn-success">View</a></td>
            <td><a href="edit.php?id=<?php echo $item['id'];?>" class="btn btn-info">Edit</a></td>
            <td><a href="delete.php?id=<?php echo $item['id'];?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete this !!!!');">Delete</a></td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
    <hr/>
</div>
</body>
</html>
