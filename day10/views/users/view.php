<?php
include_once ("../../vendor/autoload.php");
use App\users\user;

$obj = new user();
$id = $_GET['id'];
$singleView  = $obj->view($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <hr/>
    <h2 style="color: #1d78cb"><b>Profile:</b></h2>
    <hr/>
    <a href="create.php" class="btn btn-success">Add New User</a>
    <br><br>
    <hr/>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Full Name</th>
            <th>User Name</th>
            <th>Email Address</th>
            <th>Date-Time</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $singleView['fullname'] ;?></td>
                <td><?php echo $singleView['username'] ;?></td>
                <td><?php echo $singleView['email_address'] ;?></td>
                <td><?php echo $singleView['created_at'] ;?></td>
                <td><a href="index.php" class="btn btn-warning">Back</a></td>
            </tr>
        </tbody>
    </table>
    <hr/>
</div>
</body>
</html>

