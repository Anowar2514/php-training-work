<?php
include_once ("../../vendor/autoload.php");
use App\users\user;

$obj = new user();
$id = $_GET['id'];
$singleView  = $obj->view($id);
?>
<html>
<head>
    <title>Update Info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="class="row">
    <div class="col-lg-12">
        <div class="well">
            <h2 style="text-align: center"><b>Update Your Profile</b></h2>
            <form class="form-horizontal" method="post" action="update.php">
                <table class="table">
                    <tr>
                        <td> <input type="text" class="input-large span10" name="fullname" value="<?php echo $singleView['fullname'];?>">
                            <input type="hidden" class="input-large span10" name="id" value="<?php echo $singleView['id'];?>"></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="input-large span10" name="username" value="<?php echo $singleView['username'];?>"></td>
                    </tr>
                    <tr>
                     <td><input type="email" class="input-large span10" name="email_address" value="<?php echo $singleView['email_address'];?>"></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="update" value="Update" class="btn-small btn-info"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>
