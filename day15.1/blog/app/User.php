<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class User extends Model
{
    //one to one relationship
    public function passport(){
        return $this->hasOne(Passport::class)->withTimestamps();
    }
    //one to many relationship
    public function mobile(){
        return $this->hasMany(Mobile::class)->withPivot('created_at','updated_at');
    }
    //many to many relationship
    public function roles(){
        return $this->belongsToMany(role::class)->withTimestamps();
    }
}
