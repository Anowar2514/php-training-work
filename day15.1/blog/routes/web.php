<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
////------------------------------------------
////Route::get('/about', function () {
////    $about = ['This','is','about'];
////    return view('about',['me'=>$about]); // 1st route: ['']
////});
//
////Route::get('/about', function () {
////    $about = ['This','is','about'];
////    return view('about')->with(['me'=>$about]); //2nd route: with
////});
//
////Route::get('/about', function () {
////    $about = ['This','is','about'];
////    return view('about')->withMe($about); //3rd route: withMe
////});
//
////Route::get('/about', function () {
////    $about = ['This','is','about'];
////    return view('about',compact('about'));//4th route: compact
////});
//
////Route::get('/about', function () {
////    $about = [];
////    return view('about',['about'=>$about]); // use  unless
////});
//
//Route::get('/about', function () {
//    $about = ['Hello','this','is','not','unless'];
//    return view('about',['about'=>$about]); // use  unless
//});
////---------------------------------------------
//Route::get('/contact', function () {
//    return view('contact');
//});
//Route::get('/login', function () {
//    return view('login');
//});
//Route::get('/signup', function () {
//    return view('signup');
//});

Route::get('/','HomeController@index');
Route::get('/about','HomeController@about');
Route::resource('/songs','SongsController');
//Route::get('/songs/id/edit','SongsController@edit');
//Route::get('/songs/id/delete','SongsController@delete');
//Route::post('/songs/store','SongsController@store');



