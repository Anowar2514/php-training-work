<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 3/28/2018
 * Time: 3:37 AM
 */

namespace App\question;

use PDO;
class question
{

    public $question_title ="";
    public $description ="";
    public $datetime ="";
    public $question_id ="";
    public $answer = "";
    public function __construct()
    {
        session_start();
    }

    public function setQuestion($data){

        date_default_timezone_set('Asia/Dhaka');
        $this->datetime = date("y/m/d h:i:sa");
        if(!empty($data['question_title'])) {
            $this->question_title = filter_var($data['question_title'],FILTER_SANITIZE_STRING);
        }
        if(!empty($data['description'])) {
            $this->description = filter_var($data['description'],FILTER_SANITIZE_STRING);
        }
        if(!empty($data['user_id'])) {
            $this->question_id = $data['question_id'];
        }
    }
    public function setAnswer(){
        date_default_timezone_set('Asia/Dhaka');
        $this->datetime = date("y/m/d h:i:sa");
        if(!empty($data['answer'])) {
            $this->answer = filter_var($data['answer'],FILTER_SANITIZE_STRING);
        }
    }

    public function store(){
        try{
            $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
            $query = "INSERT INTO questions(question_title,description,created_at) VALUES(:question_title,:description,:created_at)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':question_title'=>$this->question_title,
                ':description'=>$this->description,
                ':created_at'=>$this->datetime
            ));
            if($stmt){
                //$_SESSION['userSuccess']='Question Successfully Added !!!!';
                header('Location:view_post.php');
            }
            else{
                echo 'Error in the database !!!';
            }
        }catch (PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }
    public function storeAnswer(){
        try{
            $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
            $query = "INSERT INTO tbl_answer(answer,created_at) VALUES(:anyanswer,:created_at)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':anyanswer'=>$this->answer,
                ':created_at'=>$this->datetime
            ));
            if($stmt){
                //$_SESSION['userSuccess']='Answer Successfully Added !!!!';
                header('Location:view_question.php');
            }
            else{
                echo 'Error in the database !!!';
            }
        }catch (PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }
    public function getAllQuestion(){
        try{
            $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
            $query = "SELECT * FROM questions WHERE status = '1'";
            $stmt = $conn->query($query);
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $data;
        }catch (PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }
    public function view($question_id=''){
        try{
            $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
            $query = "SELECT * FROM questions WHERE question_id = '$question_id'";
            $stmt = $conn->query($query);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //$this->dd($data);
            return $data;
        }catch (PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }
    public function getAllAnswer(){
        try{
            $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
            $query = "SELECT * FROM tbl_answer";
            $stmt = $conn->query($query);
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $data;
        }catch (PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }
    public function viewAnswer($answer_id=''){
        try{
            $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
            $query = "SELECT * FROM tbl_answer WHERE answer_id = '$answer_id'";
            $stmt = $conn->query($query);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //$this->dd($data);
            return $data;
        }catch (PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }
//    public function update(){
//        $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
//        $query = "UPDATE users SET fullname = :fullname,username = :username,email_address = :email_address WHERE user_id=$this->user_id";
//        $stmt = $pdo->prepare($query);
//        $stmt->execute(array(
//            ':fullname'=>$this->fullname,
//            ':username'=>$this->username,
//            ':email_address'=>$this->email_address
//        ));
//        if($stmt){
//            $_SESSION['userSuccess']='User Successfully Updated !!!!';
//            header('Location:index.php');
//        }
//        else{
//            $_SESSION['userSuccess'] = 'Update is not completed !!!';
//            header('Location:index.php');
//        }
//    }
//    public function delete($id=''){
//        $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
//        $query = "DELETE * FROM users WHERE id=$id";
//        $stmt = $pdo->query($query);
//        if($stmt){
//            $_SESSION['userSuccess'] ='User Successfully Deleted !!!!';
//            header('Location:index.php');
//        }
//        else{
//            $_SESSION['userSuccess'] = 'Delete is not completed !!!';
//            header('Location:index.php');
//        }
//    }
    public function dd($data=''){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        die();
    }
}