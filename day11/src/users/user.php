<?php

namespace App\users;
use PDO;
class user
{
   public $fullname ="";
   public $username ="";
   public $email_address="";
   public $password ="";
   public $datetime ="";
   public $user_id ="";
   public function __construct()
   {
       session_start();
   }

    public function setData($data){

    date_default_timezone_set('Asia/Dhaka');
    $this->datetime = date("y/m/d h:i:sa");
    if(!empty($data['fullname'])) {
        $this->fullname = filter_var($data['fullname'],FILTER_SANITIZE_STRING);
    }
    if(!empty($data['username'])) {
        $this->username = filter_var($data['username'],FILTER_SANITIZE_STRING);
    }
    if(!empty($data['email_address'])) {
        $this->email_address = filter_var($data['email_address'],FILTER_SANITIZE_STRING);
    }
    if(!empty($data['password'])) {
        $this->password = filter_var($data['password'],FILTER_SANITIZE_STRING);
    }
    if(!empty($data['user_id'])) {
        $this->user_id = $data['user_id'];
    }
}

public function store(){
    try{
         $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
         $query = "INSERT INTO users(fullname,username,email_address,password,created_at) VALUES(:fullname,:anyuser,:email_address,:password,:created_at)";
         $stmt = $pdo->prepare($query);
         $stmt->execute(array(
             ':fullname'=>$this->fullname,
             ':anyuser'=>$this->username,
             ':email_address'=>$this->email_address,
             ':password'=>$this->password,
             ':created_at'=>$this->datetime
         ));
         if($stmt){
             $_SESSION['userSuccess']='User Successfully Added !!!!';
             header('Location:index.php');
         }
         else{
             echo 'Error in the database !!!';
         }
     }catch (PDOException $e){
         echo 'Error:'.$e->getMessage();
     }
}
public function getAllUsers(){
       try{
        $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
        $query = "SELECT * FROM users WHERE status = '1'";
        $stmt = $conn->query($query);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }catch (PDOException $e){
        echo 'Error:'.$e->getMessage();
    }
}
public function view($user_id=''){
    try{
        $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
        $query = "SELECT * FROM users WHERE user_id = '$user_id'";
        $stmt = $conn->query($query);
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        //$this->dd($data);
        return $data;
    }catch (PDOException $e){
        echo 'Error:'.$e->getMessage();
    }
}
public function update(){
    $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
    $query = "UPDATE users SET fullname = :fullname,username = :username,email_address = :email_address WHERE user_id=$this->user_id";
    $stmt = $pdo->prepare($query);
    $stmt->execute(array(
        ':fullname'=>$this->fullname,
        ':username'=>$this->username,
        ':email_address'=>$this->email_address
    ));
    if($stmt){
        $_SESSION['userSuccess']='User Successfully Updated !!!!';
        header('Location:index.php');
    }
    else{
        $_SESSION['userSuccess'] = 'Update is not completed !!!';
        header('Location:index.php');
    }
}
    public function delete($id=''){
        $pdo = new PDO('mysql:host=localhost; dbname=cms','root','');
        $query = "DELETE * FROM users WHERE id=$id";
        $stmt = $pdo->query($query);
        if($stmt){
            $_SESSION['userSuccess'] ='User Successfully Deleted !!!!';
            header('Location:index.php');
        }
        else{
            $_SESSION['userSuccess'] = 'Delete is not completed !!!';
            header('Location:index.php');
        }
    }
public function dd($data=''){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die();
}
public function checklogin($data=''){
       $email_address = $data['email_address'];
       $password = $data['password'];
       $conn = new PDO('mysql:host=localhost; dbname=cms','root','');
       $query = "SELECT * FROM users WHERE email_address = '$email_address' AND password = '$password'";
       $stmt = $conn->query($query);
       $data = $stmt->fetch(PDO::FETCH_ASSOC);
       if(!empty($data)) {
           $_SESSION['UserData'] = $data;
           $_SESSION['loginSuccess'] = "Successfully Logged in";
           header('Location:index.php');
       }
       else{
           $_SESSION['Message'] = "Wrong User Name Or Password !!!!";
           header('Location:login.php');
       }
}
}