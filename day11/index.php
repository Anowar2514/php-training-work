<?php
include_once ("./vendor/autoload.php");
use App\question\question;
$new_obj = new question();
$data = $new_obj->getAllQuestion();
session_start();
if(!empty($_SESSION['Message'])){
    echo '<h3 style="color: red">'.$_SESSION['Message'].'<h3>';
    unset($_SESSION['Message']);
}
if(!empty($_SESSION['logout'])){
    echo '<h3 style="color: red">'.$_SESSION['logout'].'<h3>';
    unset($_SESSION['logout']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Mind Read</title>
    <link href="assets/front_end/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><b>MIND READ</b></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" method="post" action="views/users/user_login.php">
                <div class="form-group">
                    <input type="email"  name="email_address" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" name="login" class="btn btn-success">Log in</button>
                <td><a href="views/users/create.php" class="btn btn-warning">Sign Up</a></td>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>



    <div class="container">
        <hr/>
        <br><br>
        <hr/>
        <h2 style="text-align: center"><b>Question List</b></h2>
        <br><br>
        <hr/>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Serial No</th>
                <th>Question Title</th>
                <th>Posting Date</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <?php
            $sl = 1;
            foreach ($data as $item){
                ?>
                <tr>
                    <td><?php echo $sl++;?></td>
                    <td><?php echo $item['question_title'] ;?></td>
                    <td><?php echo $item['created_at'] ;?></td>
                    <td><a href="view_question.php?question_id=<?php echo $item['question_id'];?>" class="btn btn-success">Answer</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <hr/>
    </div>

    <hr>
    <footer>
        <p>&copy; 2018 Mind Read | Problem Solve.</p>
    </footer>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/assets/front_end/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/front_end/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../dist/assets/front_end/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/front_end/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
