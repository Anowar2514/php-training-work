<html>
<head>
    <title>Create</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>
<h3 style="text-align: center"><b>Post Your Question</b></h3>
<div class="container">
    <div class="class="row">
    <div class="col-lg-12">
        <div class="well">
            <form class="form-horizontal" method="post" action="store_post.php">
                <table class="table">
                    <tr>
                        <td><input type="text" class="input-large span10" name="question_title" placeholder="Enter your title name">
                            <input type="hidden" class="input-large span10" name="question_id">
                        </td>
                    </tr>
                    <tr>
                        <td><textarea name="description" class="cleditor" id="textarea2" rows="3"></textarea></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="post" value="Post" class="btn-small btn-info"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>