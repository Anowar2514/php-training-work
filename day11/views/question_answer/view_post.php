<?php
include_once ("../../vendor/autoload.php");
use App\question\question;
$new_obj = new question();
$data = $new_obj->getAllQuestion();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>List of users</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <hr/>
    <h2 style="text-align: center"><b>Question List</b></h2>

    <hr/>
    <a href="create_post.php" class="btn btn-success">Add New Question</a>
    <a href="../users/index.php" class="btn btn-success pull-right">Dashboard</a>
    <br><br>
    <hr/>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Serial No</th>
            <th>Question Title</th>
            <th>Posting Date</th>
            <th></th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 1;
        foreach ($data as $item){
            ?>
            <tr>
                <td><?php echo $sl++;?></td>
                <td><?php echo $item['question_title'] ;?></td>
                <td><?php echo $item['created_at'] ;?></td>
                <td><a href="view.php?question_id=<?php echo $item['question_id'];?>" class="btn btn-success">View</a></td>
                <td><a href="edit.php?question_id=<?php echo $item['question_id'];?>" class="btn btn-info">Edit</a></td>
                <td><a href="delete.php?question_id=<?php echo $item['question_id'];?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete this !!!!');">Delete</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <hr/>
</div>
</body>
</html>
