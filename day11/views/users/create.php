<html>
<head>
    <title>SignUp</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="class="row">
    <div class="col-lg-12">
        <div class="well">
            <hr/>
            <h1 style="text-align: center"><b>User SignUp</b></h1>
            <hr/>
<form class="form-horizontal" method="post" action="store.php">
    <table class="table">
            <tr>
            <td><input type="text" class="input-large span10" name="fullname" placeholder="Enter your full name"></td>
           </tr>
            <tr>
                <td><input type="text" class="input-large span10" name="username" placeholder="Enter your username"></td>
            </tr>
            <tr>
                <td><input type="email" class="input-large span10" name="email_address" placeholder="Enter your email address"></td>
            </tr>
              <tr>
                <td><input type="password" class="input-large span10" name="password" placeholder="Enter your password"></td>
            </tr>
            <tr>
                <td><input type="submit" name="signin" value="Sign In" class="btn-small btn-info"></td>
            </tr>
    </table>
</form>
            <hr/>
            <h4>Already have an account <a href="../../index.php">Login Here</a></h4>
            <hr/>
        </div>
    </div>
    </div>
</div>
</body>
</html>