<?php
session_start();
$user_id = $_GET['user_id'];
$title = $_SESSION['data'][$user_id]['title'];
$feature = $_SESSION['data'][$user_id]['feature'];

?>
<html>
<head>
    <title>Edit Product</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="class="row">
    <div class="col-lg-12">
        <div class="well">
            <hr/>
            <h1 style="text-align: center"><b>EDIT PRODUCT</b></h1>
            <hr/>
            <form class="form-horizontal" method="post" action="update.php">
                <table class="table">
                    <tr>
                        <td>
                            <input type="text" class="input-large span10" name="title" value="<?php echo $title; ?>" >
                            <input type="hidden" class="input-large span10" name="user_id" value="<?php echo $user_id; ?>" >
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" class="input-large span10" name="feature" value="<?php echo $feature; ?>" ></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="edit" value="Update" class="btn-small btn-info"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>