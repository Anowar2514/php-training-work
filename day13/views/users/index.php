<?php
session_start();
$data = $_SESSION['data'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>List of users</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h4>
<!--        --><?php
//        if(!empty($_SESSION['UserData']['fullname'])){
//            echo 'Welcome to the dashboard '.$_SESSION['UserData']['fullname'];
//            unset($_SESSION['UserData']['fullname']);
//        }
        ?>
    </h4>
    <hr/>
    <h2 style="text-align: center"><b>List of users</b></h2>
    <h3 style="color: darkblue">
        <?php
//       if(isset($_SESSION['userSuccess'])){
//           echo $_SESSION['userSuccess'];
//           unset($_SESSION['userSuccess']);
//       }
        ?>
    </h3>
    <hr/>
    <a href="create.php" class="btn btn-success">Add New Product</a>

    <br><br>
    <hr/>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Serial No</th>
            <th>Product Name</th>
            <th>Product Feature</th>
            <th></th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 1;
        foreach ($data as $key => $item){
        ?>
        <tr>
            <td><?php echo $sl++;?></td>
            <td><?php echo $item['title'] ;?></td>
            <td><?php echo $item['feature'] ;?></td>
            <td><a href="view.php?user_id=<?php echo $key;?>" class="btn btn-success">View</a></td>
            <td><a href="edit.php?user_id=<?php echo $key;?>" class="btn btn-info">Edit</a></td>
            <td><a href="delete.php?user_id=<?php echo $key;?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete this !!!!');">Delete</a></td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
    <hr/>
</div>
</body>
</html>
