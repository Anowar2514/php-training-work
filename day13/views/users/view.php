<?php
session_start();
//echo '<pre>';

$user_id = $_GET['user_id'];
$data = ($_SESSION['data'][$user_id]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <hr/>
    <h2 style="color: #1d78cb"><b>Profile:</b></h2>
    <hr/>
    <br><br>
    <hr/>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Product Name</th>
            <th>Product Feature</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $data['title'] ;?></td>
                <td><?php echo $data['feature'] ;?></td>
                <td><a href="index.php" class="btn btn-warning">Back</a></td>
            </tr>
        </tbody>
    </table>
    <hr/>
</div>
</body>
</html>

