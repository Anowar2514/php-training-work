<html>
<head>
    <title>Add Product</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="class="row">
    <div class="col-lg-12">
        <div class="well">
            <hr/>
            <h1 style="text-align: center"><b>ADD PRODUCT</b></h1>
            <hr/>
<form class="form-horizontal" method="post" action="store.php">
    <table class="table">
            <tr>
            <td><input type="text" class="input-large span3" name="title" placeholder="Enter your Product Name"></td>
           </tr>
            <tr>
                <td>
                <textarea name="feature" placeholder="Enter your Product Feature"></textarea>
                </td>
            </tr>
            <tr>
                <td><input type="submit" name="add" value="ADD" class="btn-small btn-info"></td>
            </tr>
    </table>
</form>
        </div>
    </div>
    </div>
</div>
</body>
</html>