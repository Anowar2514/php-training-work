<?php
namespace App\Blog;
class Blog
{
    public $firstName='Md.Anowar';
    public $lastName='Hossain';

    public function __construct($firstName='',$lastName='')
    {
         $fullName = $this->firstName.' '.$this->lastName;
        return $fullName;
    }

    public function getFullName(){
        $fullName = $this->firstName.' '.$this->lastName;
        return $fullName;
    }
}