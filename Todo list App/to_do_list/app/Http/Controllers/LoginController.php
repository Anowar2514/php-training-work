<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();
class LoginController extends Controller
{
    public function signup_form(Request $request){
        $data = array();
        $data['username'] = $request->username;
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['password'] = md5($request->password);
        $data['email'] = $request->email;
        DB::table('tbl_logins')
            ->insert($data);
        return Redirect::to('todo');
    }
    public function login_form(Request $request){
        $email = $request->email;
        $password = md5($request->password);
        $result = DB::table('tbl_logins')
            ->where('email',$email)
            ->where('password',$password)
            ->first();
        if ($result){
            Session::put('first_name',$result->first_name);
            Session::put('last_name',$result->last_name);
            Session::put('login_id',$result->login_id);
            Session::put('email',$result->email);
            return Redirect::to('todo');
        }else{
            Session::put('message','Email or Password Invalid');
            return Redirect::to('/login');
        }
    }
        public function logout(){
            Session::flush();
            return Redirect::to('/login');
        }
}
