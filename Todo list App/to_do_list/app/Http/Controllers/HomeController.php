<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\todo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class HomeController extends Controller
{
    public function index()
    {
        $todos = todo::all();
        return view('home',compact('todos'));
    }
    public function signup(){
        return view('signup');
    }
    public function login(){
        return view('login');
    }
}
