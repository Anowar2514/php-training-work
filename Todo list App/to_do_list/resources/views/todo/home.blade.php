@extends('layout.app')
@section('body')
    @include('todo.partials.dashboard_navigation')
    <div style="background-color:#17a2b8;height:800px;">
     @include('todo.partials.message')
    <div class="col-lg-6 col-lg-offset-3">
        <br>
        <center><h1><b style="color: #1c7430">TODO Lists</b></h1></center>
        <hr>
        <a href="todo/create" class="btn btn-success">Add New</a>
        <hr>
        <ul class="list-group col-lg-9">
            @foreach($todos as $todo)
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <b style="color: #004085">{{ str_limit($todo->title,15) }}</b>
                <span class="pull-right">{{$todo->created_at->diffForHumans()}}</span>
            </li>
                @endforeach
        </ul>
        <ul class="list-group col-lg-3">
            @foreach($todos as $todo)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <a href="{{ '/todo/'.$todo->id}}"><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i></a>

                    <a href="{{'/todo/'.$todo->id.'/edit'}}"><i class="glyphicon glyphicon-edit" aria-hidden="true"></i></a>
                   <a onclick="return confirm('Are You want to delete it ???');">
                    <form class="form-group pull-right" onclick ="return Confirm('Are You Sure to Delete It ???');" action="{{'/todo/'.$todo->id}}" method="post">
                        {{csrf_field()}}
                        {{ method_field('DELETE')}}
                        <button type="submit" style="border: none"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></button>
                    </form>
                   </a>

                </li>
            @endforeach
        </ul>
        <hr>
    </div>
    </div>
    @endsection