@extends('layout.app')
@section('body')
    @include('todo.partials.dashboard_navigation')
    <div style="background-color:#17a2b8;height:800px;">
    <div class="col-lg-4 col-lg-offset-4">
        <br>
    <center><h3><b style="color: #2ca02c">{{ucfirst(substr(Route::currentRouteName(),5))}} TO-DO List</b></h3></center>
        <hr><hr>
        <form class="form-horizontal" action="/todo/@yield('editId')" method="post">
            {{ csrf_field() }}
            @section('editMethod')
                @show
              <fieldset>
                <div class="form-group">
                    <label style="color: black">Title:</label>
                    <input type="text" name="title" class="form-control"  value="@yield('editTitle')" placeholder="Enter Your Title">
                    <label for="exampleTextarea" style="color: black">TO-DO List:</label>
                    <textarea class="form-control" name="body" id="exampleTextarea" rows="5" placeholder="Enter Your Todo Item">@yield('editBody')</textarea>
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="{{URL::to('todo')}}" class="btn btn-warning">Back</a>
                </div>
              </fieldset>
        </form>
        <hr>
        @include('todo.partials.errors')
        <hr>
    </div>
    </div>
    @endsection