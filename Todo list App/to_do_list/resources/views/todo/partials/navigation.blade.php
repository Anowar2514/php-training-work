<div class="nav">
    <ul class="topnav">
        <li><a class="active">ToDo List</a></li>
        <li><a  href="{{URL::to('/')}}">Home</a></li>
        <li ><a href="{{URL::to('/about')}}">About</a></li>
        <li class="right"><a href="{{URL::to('/signup')}}">Sign Up</a></li>
        <li class="right"><a href="{{URL::to('/login')}}">Log In</a></li>
    </ul>
</div>