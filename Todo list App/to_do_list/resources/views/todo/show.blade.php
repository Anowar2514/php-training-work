@extends('layout.app')
@section('body')
    @include('todo.partials.dashboard_navigation')
    <div style="background-color:#17a2b8;height:800px;">
      <div class="col-lg-offset-4 col-lg-4">
          <h3><b style="color:#2ca02c">{{ $item->title }}</b></h3>
          <hr>
          <p style="color: #0b2e13">{{$item->body}}</p>
          <a href="{{URL::to('todo')}}" class="btn btn-warning">Back</a>
      </div>
    </div>
    @endsection