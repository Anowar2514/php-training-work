<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SignUp</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('design/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('design/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('design/css/minified/core.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('design/css/minified/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('design/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('design/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('design/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('design/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('design/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('design/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('design/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('design/js/pages/login.js')}}"></script>
    <!-- /theme JS files -->

</head>
<body>
<div style="background-color:#17a2b8;">
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{URL::to('/')}}">ToDo List</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Registration form -->
                <form action="{{URL::to('/signup-form')}}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <div class="panel registration-form">
                                <div class="panel-body">
                                    <div class="text-center">
                                        <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                                        <h5 class="content-group-lg">Create account</h5>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" name="username" class="form-control" placeholder="Choose username">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-plus text-muted"></i>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="first_name" class="form-control" placeholder="First Name">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-check text-muted"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="last_name" class="form-control" placeholder="Last Name">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-check text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="password" name="password" class="form-control" placeholder="Create password">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-lock text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="email" name="email" class="form-control" placeholder="Your email">
                                                <div class="form-control-feedback">
                                                    <i class="icon-mention text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <a class="btn btn-info" href="{{URL::to('/')}}"><i class="icon-arrow-left13 position-left"></i> Back to Home</a>
                                        <a class="btn btn-success" href="{{URL::to('/login')}}"><i class="icon-arrow-left13 position-left"></i> Back to login form</a>
                                        <button type="submit" name="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Create account</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /registration form -->


                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2018. <a href="#">ToDo List</a> by <a href="#" target="_blank">Md.Anowar Hossain</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</div>
</body>
</html>
