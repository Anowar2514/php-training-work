<?php
//dashboard
Route::get('todo','todocontroller@index');
Route::resource('todo','todocontroller');
Route::get('/about',function (){
  return view('todo.about');
});
//front-end
Route::get('/', 'HomeController@index');
Route::get('/signup', 'HomeController@signup');
Route::get('/login', 'HomeController@login');

//signup & login related route
Route::post('/signup-form','LoginController@signup_form');
Route::post('/login-form','LoginController@login_form');
Route::get('/logout','LoginController@logout');

